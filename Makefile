
NAME := spooky-action-at-a-distance


all:
	echo "try 'make upload'"

upload:
	rsync -r \
		--exclude .git \
		./ \
		dreamhost:artificialworlds.net/presentations/${NAME}/
