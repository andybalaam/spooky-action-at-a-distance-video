class UserInfo(var user_id: String);

@main def hello() = view();

given UserInfo = UserInfo("rabbit");

def view()(using user: UserInfo) = {
  val uid = user.user_id;
  println(uid + "2");
}
