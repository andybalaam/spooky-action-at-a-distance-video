#include <string>
#include <iostream>

using namespace std;

class UserInfo {
public:
   static UserInfo& instance()
   {
      static UserInfo INSTANCE("rabbit");
      return INSTANCE;
   }

   UserInfo(UserInfo const&) = delete;
   UserInfo& operator=(UserInfo const&) = delete;

   string user_id;
private:
    UserInfo(const char* user_id): user_id(user_id) {}
};

int main() {
    string& uid = UserInfo::instance().user_id;
    cout << uid << "\n";
}
